{** Staff section **}
<div class="staff_member">
    <div class="portrait" style="float: right">
        {include file="common/image.tpl" images=$sd_staff.main_pair image_auto_size=true}
    </div>
    <div class="staff_member_info">
        <h3>{$sd_staff.firstname} {$sd_staff.lastname}</h3>
        <p><a href="mailto:{$sd_staff.email}">{$sd_staff.email}</a></p>
        <p><b>{$sd_staff.job_position}</b></p>
        <hr />
        <p>{$sd_staff.description nofilter}</p>
    </div>
</div>
{** Staff section **}
{** block-description:original **}
<h2 class="ty-main-title2">{__("sd_staff")}</h2>
<div class="ty-list-container with-shadow">
    {foreach from=$items item="sd_staff" key="key"}
        <div class="ty-column4">
            <div class="ty-company__item">
                <a class="row-status" href="{"sd_staff.show?sd_staff_id=`$sd_staff.sd_staff_id`"|fn_url}">
                    <div class="ty-list-image">
                        {include file="common/image.tpl" images=$sd_staff.main_pair image_auto_size=true}
                    </div>
                    <div class="ty-list-text">
                        <span class="ty-list-header">{$sd_staff.firstname} {$sd_staff.lastname}</span>{$sd_staff.job_position}
                    </div>
                </a>
            </div>
        </div>
    {/foreach}
</div>
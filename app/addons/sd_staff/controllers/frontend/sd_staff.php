<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'show') {
    $sd_staff = fn_sd_staff_get_data($_REQUEST['sd_staff_id'], DESCR_SL);

    if (empty($sd_staff)) {
        return [
            CONTROLLER_STATUS_NO_PAGE
        ];
    }

    Tygh::$app['view']->assign('sd_staff', $sd_staff);
}
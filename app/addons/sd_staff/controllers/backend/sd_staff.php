<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

    fn_trusted_vars('sd_staff', 'sd_staff_data');
    $suffix = '';

    //
    // Delete staff
    //
    if ($mode == 'm_delete') {
        foreach ($_REQUEST['sd_staff_ids'] as $v) {
            fn_sd_staff_delete_by_id($v);
        }

        $suffix = '.manage';
    }

    if (
        $mode === 'm_update_statuses'
        && !empty($_REQUEST['sd_staff_ids'])
        && is_array($_REQUEST['sd_staff_ids'])
        && !empty($_REQUEST['status'])
    ) {
        $status_to = (string) $_REQUEST['status'];

        foreach ($_REQUEST['sd_staff_ids'] as $sd_staff_id) {
            if (!fn_check_company_id('sd_staff', 'sd_staff_id', $sd_staff_id)) {
                continue;
            }
            fn_tools_update_status([
                'table'             => 'sd_staff',
                'status'            => $status_to,
                'id_name'           => 'sd_staff_id',
                'id'                => $sd_staff_id,
                'show_error_notice' => false
            ]);
        }

        if (defined('AJAX_REQUEST')) {
            $redirect_url = fn_url('sd_staff.manage');
            if (isset($_REQUEST['redirect_url'])) {
                $redirect_url = $_REQUEST['redirect_url'];
            }
            Tygh::$app['ajax']->assign('force_redirection', $redirect_url);
            Tygh::$app['ajax']->assign('non_ajax_notifications', true);
            return [
                CONTROLLER_STATUS_NO_CONTENT
            ];
        }
    }

    //
    // Add/edit staff
    //
    if ($mode == 'update') {
        $sd_staff_id = fn_sd_staff_update($_REQUEST['sd_staff_data'], $_REQUEST['sd_staff_id'], DESCR_SL);

        $suffix = ".update?sd_staff_id=$sd_staff_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['sd_staff_id'])) {
            fn_sd_staff_delete_by_id($_REQUEST['sd_staff_id']);
        }

        $suffix = '.manage';
    }

    return [
        CONTROLLER_STATUS_OK, 
        'sd_staff' . $suffix
    ];

}

if ($mode == 'update') {
    $sd_staff = fn_sd_staff_get_data($_REQUEST['sd_staff_id'], DESCR_SL);

    if (empty($sd_staff)) {
        return [
            CONTROLLER_STATUS_NO_PAGE
        ];
    }

    Registry::set('navigation.tabs', [
        'general' => [
            'title' => __('general'),
            'js' => true
        ],
    ]);

    Tygh::$app['view']->assign('sd_staff', $sd_staff);

} elseif ($mode == 'manage') {

    list($sd_staff, $params) = fn_sd_staff_get($_REQUEST, DESCR_SL, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign([
        'sd_staff'  => $sd_staff,
        'search' => $params,
    ]);
}
<?php

use Tygh\Registry;
use Tygh\BlockManager\Block;
use Tygh\Languages\Languages;
use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets staff list by search params
 *
 * @param array  $params         Staff search params
 * @param string $lang_code      2 letters language code
 * @param int    $items_per_page Items per page
 *
 * @return array Staff list and Search params
 */
function fn_sd_staff_get($params = [], $lang_code = CART_LANGUAGE, $items_per_page = 0)
{
    // Set default values to input params
    $default_params = [
        'page' => 1,
        'items_per_page' => $items_per_page
    ];

    $params = array_merge($default_params, $params);

    if (AREA == 'C') {
        $params['status'] = 'A';
    }

    $sortings = [
        'position' => '?:sd_staff.position',
        'timestamp' => '?:sd_staff.timestamp',
        'name' => 'CONCAT(?:sd_staff_descriptions.lastname, ?:sd_staff_descriptions.firstname)',
        'email' => '?:sd_staff.email',
        'job_position' => '?:sd_staff_descriptions.job_position',
        'status' => '?:sd_staff.status',
    ];

    $condition = $limit = $join = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:sd_staff.sd_staff_id IN (?n)', explode(',', $params['item_ids']));
    }

    if (!empty($params['firstname'])) {
        $condition .= db_quote(' AND ?:sd_staff_descriptions.firstname LIKE ?l', '%' . trim($params['firstname']) . '%');
    }

    if (!empty($params['lastname'])) {
        $condition .= db_quote(' AND ?:sd_staff_descriptions.lastname LIKE ?l', '%' . trim($params['lastname']) . '%');
    }

    if (!empty($params['status'])) {
        $condition .= db_quote(' AND ?:sd_staff.status = ?s', $params['status']);
    }

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(' AND (?:sd_staff.timestamp >= ?i AND ?:sd_staff.timestamp <= ?i)', $params['time_from'], $params['time_to']);
    }

    $fields = [
        '?:sd_staff.sd_staff_id',
        '?:sd_staff.email',
        '?:sd_staff.status',
        '?:sd_staff.position',
        '?:sd_staff_descriptions.firstname',
        '?:sd_staff_descriptions.lastname',
        '?:sd_staff_descriptions.job_position',
        '?:sd_staff_descriptions.description',
        '?:sd_staff_descriptions.url',
        '?:sd_staff_images.sd_staff_image_id',
    ];

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:sd_staff.company_id';
    }

    /**
     * This hook allows you to change parameters of the staff selection before making an SQL query.
     *
     * @param array        $params    The parameters of the user's query (limit, period, item_ids, etc)
     * @param string       $condition The conditions of the selection
     * @param string       $sorting   Sorting (ask, desc)
     * @param string       $limit     The LIMIT of the returned rows
     * @param string       $lang_code Language code
     * @param array        $fields    Selected fields
     */
    fn_set_hook('get_sd_staff', $params, $condition, $sorting, $limit, $lang_code, $fields);

    $join .= db_quote(' LEFT JOIN ?:sd_staff_descriptions ON ?:sd_staff_descriptions.sd_staff_id = ?:sd_staff.sd_staff_id AND ?:sd_staff_descriptions.lang_code = ?s', $lang_code);
    $join .= db_quote(' LEFT JOIN ?:sd_staff_images ON ?:sd_staff_images.sd_staff_id = ?:sd_staff.sd_staff_id AND ?:sd_staff_images.lang_code = ?s', $lang_code);

    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:sd_staff $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $sd_staff = db_get_hash_array(
        "SELECT ?p FROM ?:sd_staff " .
        $join .
        "WHERE 1 ?p ?p ?p",
        'sd_staff_id', implode(', ', $fields), $condition, $sorting, $limit
    );

    if (!empty($params['item_ids'])) {
        $sd_staff = fn_sort_by_ids($sd_staff, explode(',', $params['item_ids']), 'sd_staff_id');
    }

    $sd_staff_image_ids = array_column($sd_staff, 'sd_staff_image_id');
    $images = fn_get_image_pairs($sd_staff_image_ids, 'portrait', 'M', true, false, $lang_code);

    foreach ($sd_staff as $sd_staff_id => $element) {
        $sd_staff[$sd_staff_id]['main_pair'] = !empty($images[$element['sd_staff_image_id']]) ? reset($images[$element['sd_staff_image_id']]) : [];
    }

    fn_set_hook('get_sd_staffs_post', $sd_staff, $params);

    return [$sd_staff, $params];
}


//
// Get specific staff data
//
function fn_sd_staff_get_data($sd_staff_id, $lang_code = CART_LANGUAGE)
{
    // Unset all SQL variables
    $fields = $joins = [];
    $condition = '';

    $fields = [
        '?:sd_staff.sd_staff_id',
        '?:sd_staff.status',
        '?:sd_staff.email',
        '?:sd_staff.timestamp',
        '?:sd_staff.position',
        '?:sd_staff_descriptions.firstname',
        '?:sd_staff_descriptions.lastname',
        'CONCAT(?:sd_staff_descriptions.lastname, " ", ?:sd_staff_descriptions.firstname) as fullname',
        '?:sd_staff_descriptions.job_position',
        '?:sd_staff_descriptions.description',
        '?:sd_staff_descriptions.url',
        '?:sd_staff_images.sd_staff_image_id',
    ];

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:sd_staff.company_id as company_id';
    }

    $joins[] = db_quote("LEFT JOIN ?:sd_staff_descriptions ON ?:sd_staff_descriptions.sd_staff_id = ?:sd_staff.sd_staff_id AND ?:sd_staff_descriptions.lang_code = ?s", $lang_code);
    $joins[] = db_quote("LEFT JOIN ?:sd_staff_images ON ?:sd_staff_images.sd_staff_id = ?:sd_staff.sd_staff_id AND ?:sd_staff_images.lang_code = ?s", $lang_code);

    $condition = db_quote("WHERE ?:sd_staff.sd_staff_id = ?i", $sd_staff_id);
    $condition .= (AREA == 'A') ? '' : " AND ?:sd_staff.status IN ('A', 'H') ";

    /**
     * Prepare params for staff data SQL query
     *
     * @param int   $sd_staff_id    Staff ID
     * @param str   $lang_code      Language code
     * @param array $fields         Fields list
     * @param array $joins          Joins list
     * @param str   $condition      Conditions query
     */
    fn_set_hook('get_sd_staff_data', $sd_staff_id, $lang_code, $fields, $joins, $condition);

    $sd_staff = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:sd_staff " . implode(" ", $joins) ." $condition");

    if (!empty($sd_staff)) {
        $sd_staff['main_pair'] = fn_get_image_pairs($sd_staff['sd_staff_image_id'], 'portrait', 'M', true, false, $lang_code);
    }

    /**
     * Post processing of staff data
     *
     * @param int   $sd_staff_id    Staff ID
     * @param str   $lang_code      Language code
     * @param array $sd_staff       Staff data
     */
    fn_set_hook('get_sd_staff_data_post', $sd_staff_id, $lang_code, $sd_staff);

    return $sd_staff;
}


/**
 * Deletes staff and all related data
 *
 * @param int $sd_staff_id Staff identificator
 */
function fn_sd_staff_delete_by_id($sd_staff_id)
{
    if (!empty($sd_staff_id) && fn_check_company_id('sd_staff', 'sd_staff_id', $sd_staff_id)) {
        db_query("DELETE FROM ?:sd_staff WHERE sd_staff_id = ?i", $sd_staff_id);
        db_query("DELETE FROM ?:sd_staff_descriptions WHERE sd_staff_id = ?i", $sd_staff_id);

        fn_set_hook('delete_sd_staff', $sd_staff_id);

        Block::instance()->removeDynamicObjectData('sd_staff', $sd_staff_id);

        $sd_staff_images_ids = db_get_fields("SELECT sd_staff_image_id FROM ?:sd_staff_images WHERE sd_staff_id = ?i", $sd_staff_id);

        foreach ($sd_staff_images_ids as $sd_staff_image_id) {
            fn_delete_image_pairs($sd_staff_image_id, 'portrait');
        }

        db_query("DELETE FROM ?:sd_staff_images WHERE sd_staff_id = ?i", $sd_staff_id);
    }
}


function fn_sd_staff_update($data, $sd_staff_id, $lang_code = DESCR_SL)
{
    SecurityHelper::sanitizeObjectData('sd_staff', $data);

    if (isset($data['timestamp'])) {
        $data['timestamp'] = fn_parse_date($data['timestamp']);
    }

    $data['localization'] = empty($data['localization']) ? '' : fn_implode_localizations($data['localization']);

    if (!empty($sd_staff_id)) {
        db_query("UPDATE ?:sd_staff SET ?u WHERE sd_staff_id = ?i", $data, $sd_staff_id);
        db_query("UPDATE ?:sd_staff_descriptions SET ?u WHERE sd_staff_id = ?i AND lang_code = ?s", $data, $sd_staff_id, $lang_code);

        $sd_staff_image_id = fn_sd_staff_get_image_id($sd_staff_id, $lang_code);
        $sd_staff_image_exist = !empty($sd_staff_image_id);
        $sd_staff_is_multilang = Registry::get('addons.sd_staff.sd_staff_multilang') == 'Y';
        $image_is_update = fn_sd_staff_need_image_update();

        if ($sd_staff_is_multilang) {
            if ($sd_staff_image_exist && $image_is_update) {
                fn_delete_image_pairs($sd_staff_image_id, 'portrait');
                db_query("DELETE FROM ?:sd_staff_images WHERE sd_staff_id = ?i AND lang_code = ?s", $sd_staff_id, $lang_code);
                $sd_staff_image_exist = false;
            }
        } else {
            if (isset($data['url'])) {
                db_query("UPDATE ?:sd_staff_descriptions SET url = ?s WHERE sd_staff_id = ?i", $data['url'], $sd_staff_id);
            }
        }

        if ($image_is_update && !$sd_staff_image_exist) {
            $sd_staff_image_id = db_query("INSERT INTO ?:sd_staff_images (sd_staff_id, lang_code) VALUE(?i, ?s)", $sd_staff_id, $lang_code);
        }
        $pair_data = fn_attach_image_pairs('sd_staff_main', 'portrait', $sd_staff_image_id, $lang_code);

        if (!$sd_staff_is_multilang && !$sd_staff_image_exist) {
            fn_sd_staff_image_all_links($sd_staff_id, $pair_data, $lang_code);
        }

    } else {
        $sd_staff_id = $data['sd_staff_id'] = db_query("REPLACE INTO ?:sd_staff ?e", $data);

        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:sd_staff_descriptions ?e", $data);
        }

        if (fn_sd_staff_need_image_update()) {
            $sd_staff_image_id = db_get_next_auto_increment_id('sd_staff_images');
            $pair_data = fn_attach_image_pairs('sd_staff_main', 'portrait', $sd_staff_image_id, $lang_code);
            if (!empty($pair_data)) {
                $data_sd_staff_image = array(
                    'sd_staff_image_id' => $sd_staff_image_id,
                    'sd_staff_id'       => $sd_staff_id,
                    'lang_code'       => $lang_code
                );

                db_query("INSERT INTO ?:sd_staff_images ?e", $data_sd_staff_image);
                fn_sd_staff_image_all_links($sd_staff_id, $pair_data, $lang_code);
            }
        }
    }

    return $sd_staff_id;
}


function fn_sd_staff_get_image_id($sd_staff_id, $lang_code = DESCR_SL)
{
    return db_get_field("SELECT sd_staff_image_id FROM ?:sd_staff_images WHERE sd_staff_id = ?i AND lang_code = ?s", $sd_staff_id, $lang_code);
}


/**
 * Checks of request for need to update the staff image.
 *
 * @return bool
 */
function fn_sd_staff_need_image_update()
{
    if (!empty($_REQUEST['file_sd_staff_main_image_icon']) && is_array($_REQUEST['file_sd_staff_main_image_icon'])) {
        $image_sd_staff = reset($_REQUEST['file_sd_staff_main_image_icon']);

        if ($image_sd_staff == 'sd_staff_main') {
            return false;
        }
    }

    return true;
}


function fn_sd_staff_image_all_links($sd_staff_id, $pair_data, $main_lang_code = DESCR_SL)
{
    if (!empty($pair_data)) {
        $pair_id = reset($pair_data);

        $lang_codes = Languages::getAll();
        unset($lang_codes[$main_lang_code]);

        foreach ($lang_codes as $lang_code => $lang_data) {
            $_sd_staff_image_id = db_query("INSERT INTO ?:sd_staff_images (sd_staff_id, lang_code) VALUE(?i, ?s)", $sd_staff_id, $lang_code);
            fn_add_image_link($_sd_staff_image_id, $pair_id);
        }
    }
}

function fn_sd_staff_delete_image_pre($image_id, $pair_id, $object_type)
{
    if ($object_type == 'portrait') {
        $sd_staff_data = db_get_row("SELECT sd_staff_id, sd_staff_image_id FROM ?:sd_staff_images INNER JOIN ?:images_links ON object_id = sd_staff_image_id WHERE pair_id = ?i", $pair_id);

        if (Registry::get('addons.sd_staff.sd_staff_multilang') == 'Y') {

            if (!empty($sd_staff_data['sd_staff_image_id'])) {
                $lang_code = db_get_field("SELECT lang_code FROM ?:sd_staff_images WHERE sd_staff_image_id = ?i", $sd_staff_data['sd_staff_image_id']);

                db_query("DELETE FROM ?:common_descriptions WHERE object_id = ?i AND object_holder = 'images' AND lang_code = ?s", $image_id, $lang_code);
                db_query("DELETE FROM ?:sd_staff_images WHERE sd_staff_image_id = ?i", $sd_staff_data['sd_staff_image_id']);
            }

        } else {
            $sd_staff_image_ids = db_get_fields("SELECT object_id FROM ?:images_links WHERE image_id = ?i AND object_type = 'promo'", $image_id);

            if (!empty($sd_staff_image_ids)) {
                db_query("DELETE FROM ?:sd_staff_images WHERE sd_staff_image_id IN (?n)", $sd_staff_image_ids);
                db_query("DELETE FROM ?:images_links WHERE object_id IN (?n)", $sd_staff_image_ids);
            }
        }
    }
}

//
// Get staff name
//
function fn_sd_staff_get_fullname($sd_staff_id, $lang_code = CART_LANGUAGE)
{
    if (!empty($sd_staff_id)) {
        return db_get_field("SELECT CONCAT(lastname, ' ', firstname) FROM ?:sd_staff_descriptions WHERE sd_staff_id = ?i AND lang_code = ?s", $sd_staff_id, $lang_code);
    }

    return false;
}


if (fn_allowed_for('ULTIMATE')) {
    /**
     * Hook for check store permission
     */
    function fn_sd_staff_ult_check_store_permission($params, &$object_type, &$object_name, &$table, &$key, &$key_id)
    {
        if (Registry::get('runtime.controller') == 'sd_staff' && !empty($params['sd_staff_id'])) {
            $key = 'sd_staff_id';
            $key_id = $params[$key];
            $table = 'sd_staff';
            $object_name = fn_sd_staff_get_fullname($key_id, DESCR_SL);
            $object_type = __('sd_staff');
        }
    }

    /**
     * Hook for deleting store staff
     *
     * @param int $company_id Company id
     */
    function fn_sd_staff_delete_company($company_id)
    {
        $sd_staff_ids = db_get_fields("SELECT sd_staff_id FROM ?:sd_staff WHERE company_id = ?i", $company_id);

        foreach ($sd_staff_ids as $sd_staff_id) {
            fn_sd_staff_delete_by_id($sd_staff_id);
        }
    }
}
<?php

$schema['sd_staff'] = [
    'permissions' => 'manage_sd_staff',
    'modes' => [
        'update' => [
            'permissions' => [
                'GET' => 'view_sd_staff',
                'POST' => 'manage_sd_staff'
            ],
        ],
        'manage' => [
            'permissions' => 'view_sd_staff',
        ],
        'picker' => [
            'permissions' => 'view_sd_staff',
        ],
    ],
];
$schema['tools']['modes']['update_status']['param_permissions']['table']['sd_staff'] = 'manage_sd_staff';

return $schema;

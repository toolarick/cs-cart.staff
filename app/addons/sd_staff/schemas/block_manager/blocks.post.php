<?php

$schema['sd_staff'] = [
    'content' => [
        'items' => [
            'hide_label' => true,
            'type' => 'enum',
            'object' => 'sd_staff',
            'items_function' => 'fn_sd_staff_get',
            'fillings' => [
                'position' => [
                    'params' => [
                        'sort_by' => 'position',
                        'sort_order' => 'asc'
                    ],
                ]
            ],
        ],
    ],
    'templates' => [
        'addons/sd_staff/blocks/sd_staff.tpl' => [],
    ],
    'wrappers' => 'blocks/wrappers',
    'cache' => [
        'update_handlers' => [
            'sd_staff',
            'sd_staff_descriptions',
            'sd_staff_images',
        ],
    ],
];

return $schema;
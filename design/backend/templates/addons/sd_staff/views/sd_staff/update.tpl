{if $sd_staff}
    {assign var="id" value=$sd_staff.sd_staff_id}
{else}
    {assign var="id" value=0}
{/if}


{** Staff section **}

{$allow_save = $sd_staff|fn_allow_save_object:"sd_staff"}
{$hide_inputs = ""|fn_check_form_permissions}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit{if !$allow_save || $hide_inputs} cm-hide-inputs{/if}" name="sd_staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="sd_staff_id" value="{$id}" />
<input type="hidden" class="cm-no-hide-input" name="sd_staff_data[timestamp]" value="{$smarty.const.TIME}" />

{capture name="tabsbox"}

    <div id="content_general">
        {hook name="sd_staff:general_content"}
        
        <div class="control-group">
            <label for="elm_sd_staff_lastname" class="control-label cm-required">{__("sd_staff_lastname")}</label>
            <div class="controls">
            <input type="text" name="sd_staff_data[lastname]" id="elm_sd_staff_lastname" value="{$sd_staff.lastname}" size="25" class="input-large" /></div>
        </div>

        <div class="control-group">
            <label for="elm_sd_staff_firstname" class="control-label cm-required">{__("sd_staff_firstname")}</label>
            <div class="controls">
            <input type="text" name="sd_staff_data[firstname]" id="elm_sd_staff_firstname" value="{$sd_staff.firstname}" size="25" class="input-large" /></div>
        </div>

        <div class="control-group">
            <label for="elm_sd_staff_email" class="control-label">{__("email")}</label>
            <div class="controls">
            <input type="text" name="sd_staff_data[email]" id="elm_sd_staff_email" value="{$sd_staff.email}" size="25" class="input-large" /></div>
        </div>

        <div class="control-group">
            <label for="elm_sd_staff_job_position" class="control-label">{__("sd_staff_job_position")}</label>
            <div class="controls">
            <input type="text" name="sd_staff_data[job_position]" id="elm_sd_staff_job_position" value="{$sd_staff.job_position}" size="25" class="input-large" /></div>
        </div>

        {if "ULTIMATE"|fn_allowed_for}
            {include file="views/companies/components/company_field.tpl"
                name="sd_staff_data[company_id]"
                id="sd_staff_data_company_id"
                selected=$sd_staff.company_id
            }
        {/if}

        <div class="control-group">
            <label for="elm_sd_staff_position" class="control-label">{__("position_short")}</label>
            <div class="controls">
                <input type="text" name="sd_staff_data[position]" id="elm_sd_staff_position" value="{$sd_staff.position|default:"0"}" size="3"/>
            </div>
        </div>

        <div class="control-group" id="sd_staff_graphic">
            <label class="control-label">{__("image")}</label>
            <div class="controls">
                {include file="common/attach_images.tpl"
                    image_name="sd_staff_main"
                    image_object_type="portrait"
                    image_pair=$sd_staff.main_pair
                    image_object_id=$id
                    no_detailed=true
                    hide_titles=true
                }
            </div>
        </div>

        <div class="control-group {if $b_type == "G"}hidden{/if}" id="sd_staff_text">
            <label class="control-label" for="elm_sd_staff_description">{__("description")}:</label>
            <div class="controls">
                <textarea id="elm_sd_staff_description" name="sd_staff_data[description]" cols="35" rows="8" class="cm-wysiwyg input-large">{$sd_staff.description}</textarea>
            </div>
        </div>

        {include file="common/select_status.tpl" input_name="sd_staff_data[status]" id="elm_sd_staff_status" obj_id=$id obj=$sd_staff hidden=true}
        {/hook}
    <!--content_general--></div>

    <div id="content_addons" class="hidden clearfix">
        {hook name="sd_staff:detailed_content"}
        {/hook}
    <!--content_addons--></div>

    {hook name="sd_staff:tabs_content"}
    {/hook}

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="sd_staff_form" but_name="dispatch[sd_staff.update]"}
    {else}
        {if "ULTIMATE"|fn_allowed_for && !$allow_save}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=true}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[sd_staff.update]" but_role="submit-link" but_target_form="sd_staff_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}

</form>

{/capture}

{notes}
    {hook name="sd_staff:update_notes"}
    {__("sd_staff_details_notes", ["[layouts_href]" => fn_url('block_manager.manage')])}
    {/hook}
{/notes}

{include file="common/mainbox.tpl"
    title=($id) ? $sd_staff.fullname : __("sd_staff.new_sd_staff")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    select_languages=true}

{** Staff section **}

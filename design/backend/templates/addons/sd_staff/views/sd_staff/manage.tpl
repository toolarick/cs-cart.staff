{** Staff section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" id="sd_staff_form" name="sd_staff_form" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />
{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_sd_staff"}

{$c_url=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{$rev=$smarty.request.content_id|default:"pagination_contents_sd_staff"}
{$c_icon="<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{$c_dummy="<i class=\"icon-dummy\"></i>"}
{$sd_staff_statuses=""|fn_get_default_statuses:true}
{$has_permission = fn_check_permissions("sd_staff", "update_status", "admin", "POST")}

{if $sd_staff}

{if $has_permission}
    {hook name="sd_staff:bulk_edit"}
        {include file="addons/sd_staff/views/sd_staff/components/bulk_edit.tpl"}
    {/hook}
{/if}

<div class="table-responsive-wrapper longtap-selection">
    <table class="table table-middle table--relative table-responsive">
    <thead
        data-ca-bulkedit-default-object="true"
        data-ca-bulkedit-component="defaultObject"
    >
    <tr>
        <th width="6%" class="left mobile-hide">
            {include file="common/check_items.tpl" is_check_disabled=!$has_permission check_statuses=($has_permission) ? $sd_staff_statuses : '' }

            <input type="checkbox"
                class="bulkedit-toggler hide"
                data-ca-bulkedit-toggler="true"
                data-ca-bulkedit-disable="[data-ca-bulkedit-default-object=true]"
                data-ca-bulkedit-enable="[data-ca-bulkedit-expanded-object=true]"
            />
        </th>
        <th><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("sd_staff")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        <th><a class="cm-ajax" href="{"`$c_url`&sort_by=job_position&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("sd_staff_job_position")}{if $search.sort_by == "job_position"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
        
        {hook name="sd_staff:manage_header"}
        {/hook}

        <th width="6%" class="mobile-hide">&nbsp;</th>
        <th width="10%" class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status")}{if $search.sort_by == "status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    </tr>
    </thead>
    {foreach from=$sd_staff item=staff}
    <tr class="cm-row-status-{$staff.status|lower} cm-longtap-target"
        {if $has_permission}
            data-ca-longtap-action="setCheckBox"
            data-ca-longtap-target="input.cm-item"
            data-ca-id="{$staff.sd_staff_id}"
        {/if}
    >
        {$allow_save=$staff|fn_allow_save_object:"sd_staff"}

        {if $allow_save}
            {$no_hide_input="cm-no-hide-input"}
        {else}
            {$no_hide_input=""}
        {/if}

        <td width="6%" class="left mobile-hide">
            <input type="checkbox" name="sd_staff_ids[]" value="{$staff.sd_staff_id}" class="cm-item {$no_hide_input} cm-item-status-{$staff.status|lower} hide" /></td>
        <td class="{$no_hide_input}" data-th="{__("sd_staff")}">
            <a class="row-status" href="{"sd_staff.update?sd_staff_id=`$staff.sd_staff_id`"|fn_url}">{$staff.lastname} {$staff.firstname}</a>
            {include file="views/companies/components/company_name.tpl" object=$staff}
        </td>
        <td class="{$no_hide_input}" data-th="{__("email")}">
            <p>{$staff.email}</p>
        </td>
        <td class="{$no_hide_input}" data-th="{__("sd_staff_job_position")}">
            <p>{$staff.job_position}</p>
        </td>

        {hook name="sd_staff:manage_data"}
        {/hook}

        <td width="6%" class="mobile-hide">
            {capture name="tools_list"}
                <li>{btn type="list" text=__("edit") href="sd_staff.update?sd_staff_id=`$staff.sd_staff_id`"}</li>
            {if $allow_save}
                <li>{btn type="list" class="cm-confirm" text=__("delete") href="sd_staff.delete?sd_staff_id=`$staff.sd_staff_id`" method="POST"}</li>
            {/if}
            {/capture}
            <div class="hidden-tools">
                {dropdown content=$smarty.capture.tools_list}
            </div>
        </td>
        <td width="10%" class="right" data-th="{__("status")}">
            {include file="common/select_popup.tpl" id=$staff.sd_staff_id status=$staff.status hidden=true object_id_name="sd_staff_id" table="sd_staff" popup_additional_class="`$no_hide_input` dropleft"}
        </td>
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="pagination_contents_sd_staff"}

{capture name="adv_buttons"}
    {hook name="sd_staff:adv_buttons"}
    {include file="common/tools.tpl" tool_href="sd_staff.add" prefix="top" hide_tools="true" title=__("add_sd_staff") icon="icon-plus"}
    {/hook}
{/capture}

</form>

{/capture}

{hook name="sd_staff:manage_mainbox_params"}
    {$page_title = __("sd_staff")}
    {$select_languages = true}
{/hook}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons select_languages=$select_languages sidebar=$smarty.capture.sidebar}
{** Staff section **}